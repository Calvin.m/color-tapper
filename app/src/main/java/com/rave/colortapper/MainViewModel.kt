package com.rave.colortapper

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.random.Random

class MainViewModel : ViewModel() {

    //backing field
    //mutable means read and write
//    private val _result = MutableStateFlow(0)
    private val _result = MutableStateFlow(mutableListOf(255, 255, 255))
//    private val _rresult = MutableList("0", "0", "0")

    //observable =>
    val result: StateFlow<MutableList<Int>> get() = _result
    /*
    * var num1 = 5
    * var num2 = 10
    * val sum = num1 + num2
    * val sumB get() = num1 + num2
    *
    * num2 = 20
    * sum is still 15
    * sumB is updated to 25
    * when you use get, it reflects the expression everytime variables change
    * */

//    fun increment() {
////        _result.value = _result.value++
//        val randomValues = List(3) { Random.nextInt(0, 100) }
//// prints new sequence every time
//        println(randomValues)
////        _result.value++
//        _result.value = mutableListOf(100, 100, 100)
//    }

    fun randomizeColor() {
//        _result.value = _result.value++
        val randomValues = List(3) { Random.nextInt(0, 255) }
// prints new sequence every time
        println(randomValues)
//        _result.value++
//        _result.value = mutableListOf("100", "100", "100")
        val stringArray = randomValues.map { it }.toMutableList()
        _result.value = mutableListOf(stringArray[0], stringArray[1], stringArray[2])
    }

    fun reset() {
//        _result.value = 0
        _result.value = mutableListOf(255, 255, 255)
    }
}