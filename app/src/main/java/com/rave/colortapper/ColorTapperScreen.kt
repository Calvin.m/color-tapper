@file:OptIn(ExperimentalFoundationApi::class)

package com.rave.colortapper

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ColorTapperScreen(
    result: MutableList<Int>,
    onClick: () -> Unit,
    onLongClick: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Row(
            Modifier
                .weight(1f)
                .padding(top = 24.dp)
        ) {
            Text(
                text = "Current color picks: \n\n${result}",
                fontSize = 32.sp
            )
        }
        Row(Modifier.weight(1f)) {
            Button(
                onClick = onClick,
                modifier = Modifier
                    .height(200.dp)
                    .width(200.dp)
                    .combinedClickable(
                        onLongClick = onLongClick,
                        onClick = { },
                    )
            ) {
                Text(
                    text = "Click Me",
                    fontSize = 35.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .combinedClickable(
                            onClick = onClick,
                            onLongClick = onLongClick
                        )
                        .padding(top = 45.dp)
                )
            }
        }
        Row(Modifier.weight(1f)) {
            if (result != mutableListOf(255,255,255)) {
                Text(
                    text = "Hold button reset the color!",
                    fontSize = 24.sp
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ColorTapperScreenPreview() {
    ColorTapperScreen(result = mutableListOf(0, 0, 0), onClick = {}) {}
}