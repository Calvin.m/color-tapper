package com.rave.colortapper

import android.util.Log
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class MainViewModelTest {

    private val mainViewModel = MainViewModel()

    @Test
    @DisplayName("Array integer values should be different from default")
    fun testRandom() {
        //Given

        //When
        mainViewModel.randomizeColor()

        //Then
        if (mainViewModel.result.value != mutableListOf(255,255,255)) {
            Assertions.assertEquals(0,0)
        } else if (mainViewModel.result.value == mutableListOf(255,255,255)) {
            Assertions.assertEquals(0,1)
        }
    }


    @Test
    @DisplayName("Array integer values should equal default values")
    fun testRest() {
        //Given

        //When
        mainViewModel.randomizeColor()
        mainViewModel.reset()

        //Then
        Assertions.assertEquals(mutableListOf(255,255,255), mainViewModel.result.value)
    }

}
